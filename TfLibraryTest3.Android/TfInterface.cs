﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Org.Tensorflow.Contrib.Android;

namespace TfLibraryTest3.Android
{
    public class TfInterfaceImplementation : TfInterfaceBase
    {
        private const float IMAGE_MEAN_R = 123.0f;
        private const float IMAGE_MEAN_G = 117.0f;
        private const float IMAGE_MEAN_B = 104.0f;

        private List<string> _labels;
        private TensorFlowInferenceInterface _inferenceInterface;

        // TODO: This needs to be able to accept user input of this. Maybe read from text file?
        private static readonly int InputSize = 227;
        private static readonly string InputName = "Placeholder";
        private static readonly string OutputName = "loss";

        #region Methods definition
        public override async Task<IReadOnlyList<Classification>> ClassifyImage(Stream stream)
        {
            if (this._labels == null || !this._labels.Any() || this._inferenceInterface == null)
            { throw new InvalidOperationException("The implementation must be initialized before use by calling TfInterfaceImplementation.Initialize."); }

            using (Bitmap bitmap = await BitmapFactory.DecodeStreamAsync(stream))
            {
                // TODO: Why does this have to be done with the Task.Run? TensorFlow interface is a little strange.
                IReadOnlyList<Classification> classifications = await Task.Run(() => RecognizeImage(bitmap).AsReadOnly());
                bitmap.Recycle();
                return classifications;
            }
        }

        public override void Initialize(string modelName)
        {
            base.Initialize(modelName);

            // TODO: Should I have a try catch for error handling?
            AssetManager assets = Application.Context.Assets;
            using (var reader = new StreamReader(assets.Open("labels.txt")))
            {
                string content = reader.ReadToEnd();
                this._labels = content.Split('\n').Select(s => s.Trim()).Where(s => !string.IsNullOrEmpty(s)).ToList();
            }

            // TODO: Maybe use other overloads of the constructor? One accepts a graph directly.
            _inferenceInterface = new TensorFlowInferenceInterface(assets, modelName);
        }

        private List<Classification> RecognizeImage(Bitmap bitmap)
        {
            var outputNames = new string[] { TfInterfaceImplementation.OutputName, };
            // TODO: Set floatValues properly.
            //var floatValues = new float[] { };
            var floatValues = bitmap.GetBitmapPixels(InputSize, InputSize,
                                                     IMAGE_MEAN_R, IMAGE_MEAN_G, IMAGE_MEAN_B);
            var outputs = new float[this._labels.Count];

            this._inferenceInterface.Feed(TfInterfaceImplementation.InputName,
                                          floatValues,
                                          1,
                                          TfInterfaceImplementation.InputSize,
                                          TfInterfaceImplementation.InputSize,
                                          3);
            this._inferenceInterface.Run(outputNames);
            this._inferenceInterface.Fetch(TfInterfaceImplementation.OutputName, outputs);

            var results = new List<Classification>();
            for (int i = 0; i < outputs.Length; i++)
            { results.Add(new Classification(this._labels[i], outputs[i])); }

            return results;
        }
        #endregion Methods definition
    }
}