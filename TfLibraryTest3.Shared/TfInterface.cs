﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace TfLibraryTest3
{
//    public struct Classification
//    {
//        #region Static members definition
//        private const string TO_STRING_FORMAT = "Tag={0}, Probability={1}";
//        #endregion Static members definition

//        public string Tag { get; }
//        public double Probability { get; }

//        public Classification(string tag, double probability)
//        {
//            this.Tag = tag;
//            this.Probability = probability;
//        }

//        public override string ToString()
//        {
//            return String.Format(TO_STRING_FORMAT, this.Tag, this.Probability);
//        }
//    }

//    public interface ITfInterface
//    {
//        void Initialize(string modelName);
//        Task<IReadOnlyList<Classification>> ClassifyImage(Stream imageStream);
//    }

//    public abstract class TfInterfaceBase : ITfInterface
//    {
//        protected string ModelName { get; private set; }

//        public virtual void Initialize(string modelName)
//        {
//            if (string.IsNullOrEmpty(modelName))
//            { throw new ArgumentException("The given model name is invalid.", nameof(modelName)); }

//            this.ModelName = modelName;
//        }

//        public abstract Task<IReadOnlyList<Classification>> ClassifyImage(Stream imageStream);
//    }

//    public static class CrossPlatformTfInterface
//    {
//#if !NETSTANDARD1_0
//        private static TfInterfaceImplementation _implementation;
//#endif

//        public static ITfInterface Current
//        {
//            //            get
//            //            {
//            //#if __ANDROID__
//            //                return _implementation ?? (_implementation = (ITfInterface)(new Android.TfInterfaceImplementation()));
//            //#elif __IOS__
//            //                return _implementation ?? (_implementation = (ITfInterface)(new iOS.TfInterfaceImplementation()));
//            //#else
//            //                throw new InvalidOperationException("You must install the library into your Android and iOS projects.");
//            //#endif
//            //            }
//            get
//            {
//#if NETSTANDARD1_0
//                throw new NotImplementedException("You must install the library into your Android and iOS projects.");
//#else
//                return _implementation ?? (_implementation = new TfInterfaceImplementation());
//#endif
//            }
//        }
//    }
}
