﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreGraphics;
using CoreML;
using CoreVideo;
using Foundation;
using UIKit;
using Vision;

namespace TfLibraryTest3.iOS
{
    public class TfInterfaceImplementation : TfInterfaceBase
    {
        #region Members definition
        // TODO: This size needs to be based on user input.
        private static readonly CGSize _targetImageSize = new CGSize(227, 227);
        private VNCoreMLModel _model;
        #endregion Members definition

        #region Methods definition
        public override void Initialize(string modelName)
        {
            base.Initialize(modelName);

            this._model = this.LoadModel(modelName);
        }

        private VNCoreMLModel LoadModel(string modelName)
        {
            NSUrl modelPath = NSBundle.MainBundle.GetUrlForResource(modelName, "mlmodelc") ?? CompileModel(modelName);

            if (modelPath == null)
            { throw new ArgumentException("The given model was not found.", nameof(modelName)); }

            MLModel mlModel = MLModel.Create(modelPath, out NSError e);

            if (e != null)
            { throw new NSErrorException(e); }

            VNCoreMLModel model = VNCoreMLModel.FromMLModel(mlModel, out e);

            if (e != null)
            { throw new NSErrorException(e); }

            return model;
        }

        private NSUrl CompileModel(string modelName)
        {
            NSUrl uncompiled = NSBundle.MainBundle.GetUrlForResource(modelName, "mlmodel");
            NSUrl modelPath = MLModel.CompileModel(uncompiled, out NSError e);

            if (e != null)
            { throw new NSErrorException(e); }

            return modelPath;
        }

        private async Task<IReadOnlyList<Classification>> Classify(UIImage source)
        {
            var tcs = new TaskCompletionSource<IEnumerable<Classification>>();

            var request = new VNCoreMLRequest(this._model, (response, e) =>
            {
                if (e != null)
                { tcs.SetException(new NSErrorException(e)); }
                else
                {
                    VNClassificationObservation[] results = response.GetResults<VNClassificationObservation>();
                    tcs.SetResult(results.Select(r => new Classification(r.Identifier, r.Confidence)).ToList());
                }
            });

            CVPixelBuffer buffer = source.ToCVPixelBuffer(_targetImageSize);
            var requestHandler = new VNImageRequestHandler(buffer, new NSDictionary());

            requestHandler.Perform(new[] { request, }, out NSError error);

            IEnumerable<Classification> classifications = await tcs.Task;

            if (error != null)
            { throw new NSErrorException(error); }

            return classifications.OrderByDescending(p => p.Probability)
                                  .ToList()
                                  .AsReadOnly();
        }

        public override async Task<IReadOnlyList<Classification>> ClassifyImage(Stream stream)
        {
            if (this._model == null)
            { throw new InvalidOperationException("The implementation must be initialized before use by calling TfInterfaceImplementation.Initialize."); }

            UIImage image = await stream.ToUIImage();
            return await Classify(image);
        }
        #endregion Methods definition
    }
}